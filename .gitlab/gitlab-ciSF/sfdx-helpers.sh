#!/bin/bash

####################################################
# Assign environnement variables
####################################################
export SFDX_USE_GENERIC_UNIX_KEYCHAIN=true
export SFDX_DOMAIN_RETRY=300
export SFDX_DISABLE_APP_HUB=true
export SFDX_LOG_LEVEL=DEBUG
export SFDX_REST_DEPLOY=true
export SFDX_DISABLE_SOURCE_MEMBER_POLLING=true

####################################################
# Helpers
####################################################
# Function to authenticate to Salesforce.
# Don't expose the auth url to the logs.
# org_auth_url parameter can be obtained by reading the "Sfdx Auth Url" returned by: sfdx force:org:display --verbose
# Arguments:
#	$1 = alias to set
#	$2 = Sfdx Auth URL

function authenticate() {
	local alias_to_set=$1
	local org_auth_url=$2
	local file=$(mktemp)
	echo "authenticate"
	echo $org_auth_url > $file
	local cmd="sfdx force:auth:sfdxurl:store --sfdxurlfile $file --setalias $alias_to_set --json" && (echo $cmd >&2)
	local output=$($cmd)
	rm $file
}

# RunLocalTest
# Arguments
#	$1 = username or alias for the target org

function runLocalTest() {
	# Make directory to output test results
	# https://gitlab.com/help/ci/yaml/README.md#artifactsreports
	mkdir -p ./tests/apex
	local org_username=$1
	echo "run all test"
	sfdx force:apex:test:run -c -v -u $org_username -r junit -l RunLocalTests -w $WAIT_TIME -d ./tests/apex
}

# Generate Delta Package
# Use SFDX-Git-Delta plugin to generate a package.xml file with the added or modified metadata, and a destructiveChanges.xml file with deleted metadata
# For more information, see https://github.com/scolladon/sfdx-git-delta

function generate_delta_packages() {

	echo "sfdx sgd:source:delta -a $PACKAGE_API_VERSION -t HEAD -f $LAST_DEPLOYED_COMMIT -r . -o . -i manifest/mdapi/.sgdignore"
	sfdx sgd:source:delta -a $PACKAGE_API_VERSION -t HEAD -f $LAST_DEPLOYED_COMMIT -r . -o . -i manifest/mdapi/.sgdignore
	
	echo "--- package.xml generated with added and modified metadata since $LAST_DEPLOYED_COMMIT ---"
	cat package/package.xml
	echo
	echo "--- destructiveChanges.xml generated with deleted metadata since $LAST_DEPLOYED_COMMIT ---"
	cat destructiveChanges/destructiveChanges.xml
	echo
}



# Deploys metadata to the org. in check-only mode
# Arguments
#	$1 = username or alias for the target org
#	$2 = delta deploy mode (boolean)
#	$2 = localTest (boolean)

function deploy_check_only() {
	local org_username=$1
	local delta_deploy=$2
	local localTest=$3

	echo "Check-Only deploy to $org_username with localTest $localTest"
	
		if [ $delta_deploy != "TRUE" ]; then
			echo "Full Check-Only"
			sfdx force:source:deploy -c -u $org_username -p force-app -l RunLocalTests -w $WAIT_TIME
		else
			echo "Delta Check-Only"
			generate_delta_packages

			echo "parse package.xlm"
			local package=$(xq . < package/package.xml | jq '.Package.types | if type=="array" then .[] else . end | select(.members)')
			if [ ! "$package" ]; then
				echo "The package is empty nothing to deploy"
			else
				if [ "$localTest" = true ] ; then
					echo "Check-only with localTest"
					if [ "$org_username" = "PROD" ] ; then
						local cmd="sfdx force:source:deploy -c -u $org_username -x package/package.xml -l RunLocalTests -w $WAIT_TIME --json" && (echo $cmd >&2)
						local output=$($cmd)
						local deployId=$(jq -r ".result.id" <<< $output) # - r command remove the "" status
						local deployStatus=$(jq -r ".status" <<< $output)

						echo $deployId
						echo $deployStatus
						
						if [ $deployStatus = 1 ]; then
							echo "ERROR during the deploy. Check the errors on production org" >&2
							exit 1;
						else
						echo DEPLOY_ID=$deployId >> build.env
						fi
					else
						sfdx force:source:deploy -c -u $org_username -x package/package.xml -l RunLocalTests -w $WAIT_TIME
					fi
				else
					echo "parse package.xlm to extract the class use for RunSpecifiedTests"
					local extractTestClass=$(xq . < package/package.xml | jq '.Package.types | if type=="array" then .[] else . end | select(.name=="ApexClass") | .members | join(",")')
					
					# remove "" from cmd result
					local classToTest=`sed -e 's/^"//' -e 's/"$//' <<<"$extractTestClass"`

					if [ ! $classToTest ]; then
						echo "---- CheckOnly with NoTestRun  ----"
						sfdx force:source:deploy -c -u $org_username -x package/package.xml -l NoTestRun -w $WAIT_TIME
					else
						echo "---- CheckOnly with runSpecifiedTest :  $classToTest   ----"
						sfdx force:source:deploy -c -u $org_username -x package/package.xml -l RunSpecifiedTests -r $classToTest -w $WAIT_TIME
					fi
				fi
			fi
		fi
}

# QuickDeploy metadata to the org.
# Arguments
#	$1 = org username to deploy to
#	s2 = id deploy to use for the quickDeploy

function quick_deploy() {
	local org_username=$1
	local deployId=$2

	sfdx force:source:deploy -q $deployId -u $org_username

}

# Deploys metadata to the org.
# Arguments
#	$1 = org username to deploy to
#	$2 = test level
#	$3 = delta deploy mode (boolean)

function deploy() {

	local org_username=$1
	local test_level=$2
	local delta_deploy=$3

	if [ $delta_deploy != "TRUE" ]; then
		echo "Full Deploy mode"
		sfdx force:source:deploy -u $org_username -p force-app -l $test_level -w $WAIT_TIME
	else
		echo "Delta Deploy mode"
		generate_delta_packages

		echo "parse package.xlm"
		local package=$(xq . < package/package.xml | jq '.Package.types | if type=="array" then .[] else . end | select(.members)')
		if [ ! "$package" ]; then
			echo "The package is empty nothing to deploy"
		else
			echo "---- Deploying with NoTestRun  ----"
			sfdx force:source:deploy -u $org_username -x package/package.xml -l NoTestRun -w $WAIT_TIME
		fi
	fi
	# delete_Metadata $org_username
	deploy-destructive $org_username
}

# Deploys destructive metadata

function deploy-destructive() {

	local org_username=$1

	# delete_Metadata $org_username
	if [ $DELTA_DESTRUCTIVE = "TRUE" ]; then
		if [ "$org_username" = "PROD" ] ; then
			generate_delta_packages
		fi
		echo "parse destructivePackage.xlm"
		local destructPackage=$(xq . < destructiveChanges/destructiveChanges.xml | jq '.Package.types | if type=="array" then .[] else . end | select(.members)')
		if [ ! "$destructPackage" ]; then
			echo "The destructPackage is empty nothing to deploy"
		else
			echo "--- Deleting removed metadata ---"
			sfdx force:mdapi:deploy -u $org_username -d destructiveChanges -w $WAIT_TIME --ignorewarnings
		fi
	fi
}

# Send Notification.
# Arguments
#	$1 = comit message
#	$2 = org to deploy
#	$3 = jobs status

function send_notification() {
	local org=$1
	local commitMessage=$2
	local jobStatus=$3
	echo "SendCustomNotification"
	curl -H 'Content-Type: application/json' -d '{
	"@type": "MessageCard",
	"@context": "http://schema.org/extensions",
	"themeColor": "0076D7",
	"summary": "notification",
	"sections": [{
			"activityTitle": "'"$commitMessage"'",
			"activitySubtitle": "deploy on '"$org"' '"$jobStatus"'",
			"activityImage": "https://teamsnodesample.azurewebsites.net/static/img/image5.png",
			"markdown": true
	}]
}' $WEBHOOKURL

}