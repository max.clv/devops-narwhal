### Description

### Post ou pre manuel action pour le deploiement

### Add Post Action for difference between sandbox and prod
- Verify if Custom label API Quote Id has not been scrashed with this deployment
- Verify New button on Account. this button shall be default button for all sandbox and overrided on Production

### Merge request check

- [ ] Les classes et test classes associées sont bien dans la Merge Request
- [ ] Le tag de revue basic ou complex est bien présents (par default basic)
- [ ] La description de la Merge Request est claire
- [ ] Les tests de non regression sont bien passés sur la branche
- [ ] l'approval rule est il bien configuré

/label ~"revueCGI::basic"
/assign_reviewer @ophelie.mayo.p @nicolas.guigui.p @maxime.clavel.c @frederik.belondrade.e @Xavier.bouhier.e
